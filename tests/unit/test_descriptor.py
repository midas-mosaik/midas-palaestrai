import os
import unittest

import mosaik

from midas_palaestrai.descriptor import Descriptor

try:
    import pandapower as pp

    PANDAPOWER_AVAILABLE = True
except ImportError:
    PANDAPOWER_AVAILABLE = False

try:
    import palaestrai
    from palaestrai.types.space import Space

    PALAESTRAI_AVAILABLE = True
except ImportError:
    PALAESTRAI_AVAILABLE = False


class TestDescriptor(unittest.TestCase):
    def setUp(self):
        self.descriptor = Descriptor()

    def test_describe(self):
        sen, act, wstate = self.descriptor.describe()

        self.assertIsInstance(sen, list)
        for s in sen:
            self.assertIsInstance(s, dict)
            self.assertIn("uid", s)
            self.assertIn("space", s)
            if PALAESTRAI_AVAILABLE:
                space = Space.from_string(s["space"])
                self.assertIsInstance(space, Space)
            else:
                print(
                    "Skipping space test because palaestrAI "
                    "is not installed"
                )

        for a in act:
            self.assertIsInstance(a, dict)
            self.assertIn("uid", a)
            self.assertIn("space", a)
            if PALAESTRAI_AVAILABLE:
                space = Space.from_string(a["space"])
                self.assertIsInstance(space, Space)
            else:
                print(
                    "Skipping space test because palaestrAI "
                    "is not installed"
                )

        self.assertIsInstance(act, list)
        self.assertIsInstance(wstate, dict)
        if PANDAPOWER_AVAILABLE:
            with open("tmp_json_file.json", "w") as jf:
                jf.write(wstate["grid_json"])
            grid = pp.from_json("tmp_json_file.json")
            os.remove("tmp_json_file.json")

            self.assertIsInstance(grid, pp.pandapowerNet)
        else:
            print(
                "Skipping loading grid because pandapower " "is not installed"
            )

    def test_get_world(self):
        sen, act, wstate = self.descriptor.describe()
        world, entities = self.descriptor.get_world()

        self.assertIsInstance(world, mosaik.World)

        world.shutdown()


if __name__ == "__main__":
    unittest.main()
